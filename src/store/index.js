import Vuex from "vuex";
import Vue from "vue";

// Don't know where this folder is, it just appeared when I type (-_-!)
import todos from "./modules/todos";

Vue.use(Vuex);

// Create store
export default new Vuex.Store({
  modules: {
    todos
  }
});
